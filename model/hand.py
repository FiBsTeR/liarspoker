"""
Contains the definition of the Hand class, as well as the HandType enum.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class HandType(object):
  """
  An enumeration of the types of poker hands, in increasing order of strength.
  """
  HIGH_CARD = 0
  PAIR = 1
  TWO_PAIR = 2
  THREE_OF_A_KIND = 3
  STRAIGHT = 4
  FLUSH = 5
  FULL_HOUSE = 6
  FOUR_OF_A_KIND = 7
  STRAIGHT_FLUSH = 8


class Hand(object):
  """
  An abstract representation of a poker hand.

  Attributes:
    hand_type: A HandType, representing the type of this hand.

  Requires:
    If two hands have the same hand_type, they must both be instances of the
    same subclass of Hand.
  """

  def __init__(self):
    """ Abstract constructor. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')

  def beats(self, other_hand):
    """ Returns whether this hand beats the other hand. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')

  def is_present(self, cards):
    """
    Returns whether this hand is present within an array of cards. Must be
    overridden.
    """
    raise Exception('Inappropriate call to abstract method.')
