"""
Contains unit tests for the TwoPairHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela), maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class TwoPairHandTest(HandTestCase):
  """
  Unit tests for TwoPairHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = TwoPairHand(7, 10)

  def testConstructor(self):
    """
    Tests that the constructor correctly sets small_value and big_value to be
    the smaller and bigger of the given arguments, respectively.
    """
    hand = TwoPairHand(4, 7)
    self.assertEquals(4, hand.small_value)
    self.assertEquals(7, hand.big_value)

    hand = TwoPairHand(7, 4)
    self.assertEquals(4, hand.small_value)
    self.assertEquals(7, hand.big_value)

  def testConstructor_equalValues(self):
    """
    Tests that the constructor throws an assertion error if given two equal
    values.
    """
    self.assertRaises(AssertionError, TwoPairHand, 3, 3)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(12), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(13), True)

  def testBeats_smallerBigValue(self):
    """
    Tests that beats returns true for a two pair hand with a smaller big value.
    """
    self._testBeats(TwoPairHand(3, 9), True)
    self._testBeats(TwoPairHand(9, 8), True)

  def testBeats_sameBigValueSmallerSmallValue(self):
    """
    Tests that beats returns true for a two pair hand with the same big value
    but smaller small value.
    """
    self._testBeats(TwoPairHand(3, 7), True)

  def testBeats_sameTwoPair(self):
    """
    Tests that beats returns false for the same two pair hand.
    """
    self._testBeats(TwoPairHand(10, 7), False)

  def testBeats_sameBigValueBiggerSmallValue(self):
    """
    Tests that beats returns false for a two pair hand with the same big value
    but bigger small value.
    """
    self._testBeats(TwoPairHand(8, 10), False)

  def testBeats_biggerBigValue(self):
    """
    Tests that beats returns false for a two pair hand with a bigger value.
    """
    self._testBeats(TwoPairHand(2, 13), False)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns false for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(10), False)

  def testBeats_straight(self):
    """ Tests that beats returns false for a straight hand. """
    self._testBeats(StraightHand(8), False)

  def testBeats_flush(self):
    """ Tests that beats returns false for a flush hand. """
    self._testBeats(FlushHand(9, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresent(self):
    """ Tests that is_present returns true if the hand is present. """
    self.assertTrue(self.hand.is_present([
        Card('Tc'), Card('Ts'), Card('7c'), Card('7h')]))
    self.assertTrue(self.hand.is_present([
        Card('Tc'), Card('Ts'), Card('7c'), Card('7h'), Card('7d')]))
    self.assertTrue(self.hand.is_present([
        Card('Tc'), Card('Ts'), Card('Th'), Card('7c'), Card('7h')]))

  def testIsPresent_notPresent(self):
    """ Tests that is_present returns false if the hand is not present. """
    self.assertFalse(self.hand.is_present([
        Card('Tc'), Card('7c'), Card('7h')]))
    self.assertFalse(self.hand.is_present([
        Card('Tc'), Card('7c'), Card('Th')]))
    self.assertFalse(self.hand.is_present([
        Card('5c'), Card('5s'), Card('4d'), Card('4s')]))
    self.assertFalse(self.hand.is_present([
        Card('5c'), Card('5s'), Card('7d'), Card('7s')]))
    self.assertFalse(self.hand.is_present([]))
