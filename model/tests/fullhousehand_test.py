"""
Contains unit tests for the FullHouseHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class FullHouseHandTest(HandTestCase):
  """
  Unit tests for FullHouseHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = FullHouseHand(5, 9)

  def testConstructor_equalValues(self):
    """
    Tests that the constructor throws an assertion error if given two equal
    values.
    """
    self.assertRaises(AssertionError, FullHouseHand, 5, 5)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(9), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(13), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(12, 11), True)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns true for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(3), True)

  def testBeats_straight(self):
    """ Tests that beats returns true for a straight hand. """
    self._testBeats(StraightHand(5), True)

  def testBeats_flush(self):
    """ Tests that beats returns true for a flush hand. """
    self._testBeats(FlushHand(13, 'h'), True)

  def testBeats_smallerTripleSmallerPair(self):
    """
    Tests that beats returns true for a full house with smaller triple and pair.
    """
    self._testBeats(FullHouseHand(4, 8), True)

  def testBeats_smallerTripleBiggerPair(self):
    """
    Tests that beats returns true for a full house with smaller triple and
    bigger pair.
    """
    self._testBeats(FullHouseHand(4, 10), True)

  def testBeats_sameTripleSmallerPair(self):
    """
    Tests that beats returns true for a full house with same triple and smaller
    pair.
    """
    self._testBeats(FullHouseHand(5, 8), True)

  def testBeats_sameFullHouse(self):
    """
    Tests that beats returns false for a full house with same triple and pair.
    """
    self._testBeats(FullHouseHand(5, 9), False)

  def testBeats_sameTripleBiggerPair(self):
    """
    Tests that beats returns false for a full house with same triple but bigger
    pair.
    """
    self._testBeats(FullHouseHand(5, 10), False)

  def testBeats_biggerTripleSmallerPair(self):
    """
    Tests that beats returns false for a full house with bigger triple despite a
    smaller pair.
    """
    self._testBeats(FullHouseHand(6, 2), False)

  def testBeats_biggerTripleBiggerPair(self):
    """
    Tests that beats returns false for a full house with bigger triple and pair.
    """
    self._testBeats(FullHouseHand(13, 12), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresent(self):
    """ Tests that is_present returns true if the full house is present. """
    self.assertTrue(self.hand.is_present([
        Card('5s'), Card('9c'), Card('9s'), Card('5d'), Card('5h')]))
    self.assertTrue(self.hand.is_present([
        Card('9c'), Card('9h'), Card('9d'), Card('9s'), Card('5s'), Card('5d'),
        Card('5c'), Card('4h')]))

  def testIsPresent_isNotPresent(self):
    """
    Tests that is_present returns false if the full house is not present.
    """
    self.assertFalse(self.hand.is_present([
        Card('5s'), Card('5c'), Card('9s'), Card('9c'), Card('9d'),
        Card('9h')]))
    self.assertFalse(self.hand.is_present([
        Card('5h'), Card('5d'), Card('5h'), Card('5s'), Card('9c')]))
    self.assertFalse(self.hand.is_present([]))
