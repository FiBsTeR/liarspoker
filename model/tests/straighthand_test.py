"""
Contains unit tests for the StraightHand class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class StraightHandTest(HandTestCase):
  """
  Unit tests for StraightHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = StraightHand(9)

  def testConstructor_invalidHighCard(self):
    """
    Tests that the constructor raises an assertion error with an invalid high
    card.
    """
    self.assertRaises(AssertionError, StraightHand, 4)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(12), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(2), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(5, 8), True)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns true for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(7), True)

  def testBeats_smallerStraight(self):
    """ Tests that beats returns true for a smaller straight hand. """
    self._testBeats(StraightHand(7), True)

  def testBeats_sameStraight(self):
    """ Tests that beats returns false for the same straight hand. """
    self._testBeats(StraightHand(9), False)

  def testBeats_biggerStraight(self):
    """ Tests that beats returns false for a bigger straight hand. """
    self._testBeats(StraightHand(11), False)

  def testBeats_flush(self):
    """ Tests that beats returns false for a flush hand. """
    self._testBeats(FlushHand(9, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresentRegular(self):
    """
    Tests that is_present returns true for a set of cards with 5 sequential
    values.
    """
    cards = [Card('6s'), Card('7c'), Card('8h'), Card('9s'), Card('5s')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isPresentLongStraight(self):
    """
    Tests that is_present returns true for a set of cards with more than 5
    sequential values.
    """
    cards = [Card('9s'), Card('7c'), Card('8h'), Card('6s'), Card('5s'),
        Card('Ts')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isPresentDuplicates(self):
    """
    Tests that is_present returns true for a set of cards with duplicates in the
    straight.
    """
    cards = [Card('9s'), Card('7c'), Card('8h'), Card('6s'), Card('5s'),
        Card('8s')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_lowAceStraight(self):
    """
    Tests that is_present returns true for a low ace straight.
    """
    cards = [Card('As'), Card('2c'), Card('3h'), Card('4s'), Card('5s')]
    self.assertTrue(StraightHand(5).is_present(cards))

  def testIsPresent_highAceStraight(self):
    """
    Tests that is_present returns true for a high ace straight.
    """
    cards = [Card('As'), Card('Kc'), Card('Qh'), Card('Js'), Card('Ts')]
    self.assertTrue(StraightHand(14).is_present(cards))

  def testIsPresent_isNotPresent(self):
    """
    Tests that is_present returns false for a set of cards not containing the
    hand.
    """
    self.assertFalse(self.hand.is_present([
        Card('2s'), Card('3s'), Card('4s'), Card('5h'), Card('7h')]))
    self.assertFalse(self.hand.is_present([
        Card('2s'), Card('3s'), Card('4s'), Card('5h'), Card('6h')]))
    self.assertFalse(self.hand.is_present([
        Card('As'), Card('2s'), Card('3s'), Card('4h'), Card('5h')]))

  def testIsPresent_empty(self):
    """
    Tests that is_present returns false for an empty set of cards.
    """
    self.assertFalse(self.hand.is_present([]))
