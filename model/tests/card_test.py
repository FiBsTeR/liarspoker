"""
Contains unit tests for the Card class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from unittest import TestCase


class TestCard(TestCase):
  """
  Unit tests for Card.
  """

  def setUp(self):
    """ Sets up for tests. """
    pass

  def testConstructor_numericValue(self):
    """
    Tests that a card is successfully instantiated for a card with a numeric
    value.
    """
    for value in range(2, 10):
      for suit in ['d', 'c', 's', 'h']:
        card = Card('%d%s' % (value, suit))
        self.assertTrue(card.value == value)
        self.assertTrue(card.suit == suit)

  def testConstructor_nonNumericValue(self):
    """
    Tests that a card is successfully instantiated for a card with a non-numeric
    value.
    """
    for suit in ['d', 'c', 's', 'h']:
      card = Card('T%s' % suit)
      self.assertTrue(card.value == 10)
      self.assertTrue(card.suit == suit)
      card = Card('J%s' % suit)
      self.assertTrue(card.value == 11)
      self.assertTrue(card.suit == suit)
      card = Card('Q%s' % suit)
      self.assertTrue(card.value == 12)
      self.assertTrue(card.suit == suit)
      card = Card('K%s' % suit)
      self.assertTrue(card.value == 13)
      self.assertTrue(card.suit == suit)
      card = Card('A%s' % suit)
      self.assertTrue(card.value == 14)
      self.assertTrue(card.suit == suit)

  def testConstructor_invalidDescription(self):
    """
    Tests that the constructor throws an exception for invalid card
    descriptions.
    """
    self.assertRaises(AssertionError, Card, '10s')
    self.assertRaises(AssertionError, Card, 'Zc')
    self.assertRaises(AssertionError, Card, '9g')

  def testClone_sameValueAndSuit(self):
    """ Tests that clone returns a card with the same value and suit.  """
    card = Card('4c')
    clone = card.clone()
    self.assertTrue(clone.value == 4)
    self.assertTrue(clone.suit == 'c')

  def testClone_notEqual(self):
    """ Tests the clone returns a different card than the original. """
    card = Card('4c')
    clone = card.clone()
    self.assertFalse(card == clone)
