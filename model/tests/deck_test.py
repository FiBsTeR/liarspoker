"""
Contains unit tests for the Deck class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from liarspoker.model.deck import Deck
from unittest import TestCase


class TestDeck(TestCase):
  """
  Unit tests for Deck.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.deck = Deck()

  def testConstructor_numCards(self):
    """
    Tests that, upon construction, the correct number of cards are in the deck.
    """
    self.assertEquals(len(Card.VALUES) * len(Card.SUITS), len(self.deck.cards))

  def testDraw_numCards(self):
    """
    Tests that, upon drawing, a card is removed from the deck.
    """
    total_num_cards = len(Card.VALUES) * len(Card.SUITS)
    expected_num_cards = total_num_cards
    for num_draws in range(total_num_cards):
      self.deck.draw()
      expected_num_cards -= 1
      self.assertEquals(expected_num_cards, len(self.deck.cards))

  def testDraw_emptyDeck(self):
    """
    Tests that drawing from an empty deck raises an exception.
    """
    total_num_cards = len(Card.VALUES) * len(Card.SUITS)
    for num_draws in range(total_num_cards):
      self.deck.draw()
    self.assertRaises(AssertionError, self.deck.draw)
