"""
Contains unit tests for the StraightFlushHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class StraightFlushHandTest(HandTestCase):
  """
  Unit tests for StraightFlushHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = StraightFlushHand(10, 's')

  def testConstructor_invalidHighCard(self):
    """
    Tests that the constructor raises an assertion error for an invalid value.
    """
    self.assertRaises(AssertionError, StraightFlushHand, 4, 's')

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(14), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(14), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(14, 13), True)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns true for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(14), True)

  def testBeats_straight(self):
    """ Tests that beats returns true for a straight hand. """
    self._testBeats(StraightHand(14), True)

  def testBeats_flush(self):
    """ Tests that beats returns true for a flush hand. """
    self._testBeats(FlushHand(14, 's'), True)

  def testBeats_fullHouse(self):
    """ Tests that beats returns true for a full house hand. """
    self._testBeats(FullHouseHand(14, 13), True)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns true for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(14), True)

  def testBeats_smallerStraightFlush(self):
    """ Tests that beats returns true for a smaller straight flush. """
    for suit in ['h', 'c', 'd', 's']:
      self._testBeats(StraightFlushHand(9, suit), True)

  def testBeats_sameStraightFlush(self):
    """
    Tests that beats returns false for a straight flush with the same value.
    """
    for suit in ['h', 'c', 'd', 's']:
      self._testBeats(StraightFlushHand(10, suit), False)

  def testBeats_biggerStraightFlush(self):
    """
    Tests that beats returns false for a bigger straight flush. """
    for suit in ['h', 'c', 'd', 's']:
      self._testBeats(StraightFlushHand(11, suit), False)

  def testIsPresent_isPresent(self):
    """ Tests that is_present returns true if the hand is present. """
    self.assertTrue(self.hand.is_present([
        Card('6s'), Card('7s'), Card('8s'), Card('9s'), Card('Ts')]))
    self.assertTrue(self.hand.is_present([
        Card('9s'), Card('Ah'), Card('6s'), Card('4s'), Card('7s'), Card('Ts'),
        Card('4c'), Card('8s'), Card('9s')]))

  def testIsPresent_notPresent(self):
    """ Tests that is_present return false if the hand is not present. """
    self.assertFalse(self.hand.is_present([
        Card('5s'), Card('6s'), Card('7s'), Card('8s'), Card('9s')]))
    self.assertFalse(self.hand.is_present([
        Card('Js'), Card('Ts'), Card('9s'), Card('8s'), Card('7s')]))
    self.assertFalse(self.hand.is_present([
        Card('6h'), Card('7h'), Card('8h'), Card('9h'), Card('Th')]))
    self.assertFalse(self.hand.is_present([]))
