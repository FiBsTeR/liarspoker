"""
Contains unit tests for the ThreeOfAKindHand class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class ThreeOfAKindHandTest(HandTestCase):
  """
  Unit tests for ThreeOfAKindHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = ThreeOfAKindHand(7)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(10), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(3), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(3, 8), True)

  def testBeats_smallerThreeOfAKind(self):
    """ Tests that beats returns true for a smaller three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(3), True)

  def testBeats_sameThreeOfAKind(self):
    """ Tests that beats returns false for the same three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(7), False)

  def testBeats_biggerThreeOfAKind(self):
    """ Tests that beats returns false for a bigger three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(11), False)

  def testBeats_straight(self):
    """ Tests that beats returns false for a straight hand. """
    self._testBeats(StraightHand(8), False)

  def testBeats_flush(self):
    """ Tests that beats returns false for a flush hand. """
    self._testBeats(FlushHand(9, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresent(self):
    """
    Tests that is_present returns true for a set of cards containing exactly
    three of the value.
    """
    cards = [Card('7s'), Card('7c'), Card('7h'), Card('3s'), Card('4s')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isPresentMoreThanThrice(self):
    """
    Tests that is_present returns true for a set of cards containing more than
    three cards of the value.
    """
    cards = [Card('7s'), Card('7c'), Card('7h'), Card('7d')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isNotPresent(self):
    """
    Tests that is_present returns false for a set of cards that does not contain
    three of a particular value.
    """
    self.assertFalse(self.hand.is_present([Card('Ts'), Card('Th')]))
    self.assertFalse(self.hand.is_present([Card('4s'), Card('4c'), Card('4h')]))

  def testIsPresent_empty(self):
    """
    Tests that is_present returns false for an empty set of cards.
    """
    self.assertFalse(self.hand.is_present([]))
