"""
Contains unit tests for the HighCardHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela), maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class HighCardHandTest(HandTestCase):
  """
  Unit tests for HighCardHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = HighCardHand(5)

  def testBeats_smallerHighCard(self):
    """ Tests that beats returns true for a smaller high card hand. """
    self._testBeats(HighCardHand(2), True)

  def testBeats_sameHighCard(self):
    """ Tests that beats returns false for the same high card hand. """
    self._testBeats(HighCardHand(5), False)

  def testBeats_biggerHighCard(self):
    """ Tests that beats returns false for a bigger high card hand. """
    self._testBeats(HighCardHand(9), False)

  def testBeats_pair(self):
    """ Tests that beats returns false for a pair hand. """
    self._testBeats(PairHand(14), False)

  def testBeats_twoPair(self):
    """ Tests that beats returns false for a two pair hand. """
    self._testBeats(TwoPairHand(4, 6), False)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns false for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(7), False)

  def testBeats_straight(self):
    """ Tests that beats returns false for a straight hand. """
    self._testBeats(StraightHand(8), False)

  def testBeats_flush(self):
    """ Tests that beats returns false for a flush hand. """
    self._testBeats(FlushHand(9, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(10, 'c'), False)

  def testIsPresent_isPresent(self):
    """
    Tests that is_present returns true for a set of cards containing the high
    card.
    """
    self.assertTrue(self.hand.is_present([
        Card('Th'), Card('2s'), Card('5c'), Card('2d')]))
    self.assertTrue(self.hand.is_present([
        Card('Th'), Card('As'), Card('5c'), Card('2d')]))

  def testIsPresent_isNotPresent(self):
    """
    Tests that is_present returns false for a set of cards not containing the
    high card.
    """
    cards = [Card('Qh'), Card('Js'), Card('Ac'), Card('2d')]
    self.assertFalse(self.hand.is_present(cards))

  def testIsPresent_empty(self):
    """
    Tests that is_present returns false for an empty set of cards.
    """
    self.assertFalse(self.hand.is_present([]))
