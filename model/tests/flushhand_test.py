"""
Contains unit tests for the FlushHand class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class FlushHandTest(HandTestCase):
  """
  Unit tests for FlushHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = FlushHand(8, 'h')

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(3), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(10), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(7, 2), True)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns true for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(7), True)

  def testBeats_straight(self):
    """ Tests that beats returns true for a straight hand. """
    self._testBeats(StraightHand(7), True)

  def testBeats_smallerFlush(self):
    """ Tests that beats returns true for a smaller flush hand. """
    self._testBeats(FlushHand(7, 's'), True)

  def testBeats_sameFlush(self):
    """ Tests that beats returns false for the same flush hand. """
    self._testBeats(FlushHand(8, 'c'), False)

  def testBeats_biggerFlush(self):
    """ Tests that beats returns false for a bigger flush hand. """
    self._testBeats(FlushHand(10, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresent(self):
    """
    Tests that is_present returns true for a set of cards with 5 of the same
    suit.
    """
    cards = [Card('6h'), Card('3h'), Card('8h'), Card('4h'), Card('5h')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isNotPresentNoFlush(self):
    """
    Tests that is_present returns false for a set of cards not containing 5 of
    the same suit.
    """
    cards = [Card('2s'), Card('3s'), Card('4s'), Card('5h'), Card('6h')]
    self.assertFalse(self.hand.is_present(cards))

  def testIsPresent_isNotPresentDifferentSuit(self):
    """
    Tests that is_present returns false for a set of cards containing 5 of the
    same suit, but not the same suit as the flush hand.
    """
    cards = [Card('4s'), Card('2s'), Card('5h'), Card('6h'), Card('8h')]
    self.assertFalse(self.hand.is_present(cards))

  def testIsPresent_isNotPresentTooLow(self):
    """
    Tests that is_present returns false for a set of cards with 5 of the same
    suit, but the high card of the flush is too low.
    """
    cards = [Card('6h'), Card('3h'), Card('5h'), Card('4h'), Card('7h')]
    self.assertFalse(self.hand.is_present(cards))

  def testIsPresent_isNotPresentTooHigh(self):
    """
    Tests that is_present returns false for a set of cards with 5 of the same
    suit, but the high card of the flush is too high.
    """
    cards = [Card('6h'), Card('3h'), Card('8h'), Card('4h'), Card('9h')]
    self.assertFalse(self.hand.is_present(cards))

  def testIsPresent_empty(self):
    """
    Tests that is_present returns false for an empty set of cards.
    """
    self.assertFalse(self.hand.is_present([]))
