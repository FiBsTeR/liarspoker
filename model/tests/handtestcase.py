"""
Contains the definition of the HandTestCase class.
"""

__author__ = 'maxnelson@mit.edu (Max Nelson)'

from unittest import TestCase


class HandTestCase(TestCase):
  """
  Superclass for testing hands.
  """

  def _testBeats(self, other_hand, wins):
    """
    Asserts that self.hand beats the other hand if wins is True, or
    doesn't beat the other hand otherwise.

    Assumes that self.hand is set by the setUp() method before the _testBeats
    method is called.
    """
    self.assertEquals(wins, self.hand.beats(other_hand))
