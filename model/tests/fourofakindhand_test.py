"""
Contains unit tests for the FourOfAKindHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class FourOfAKindHandTest(HandTestCase):
  """
  Unit tests for FourOfAKindHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = FourOfAKindHand(4)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(4), True)

  def testBeats_pair(self):
    """ Tests that beats returns true for a pair hand. """
    self._testBeats(PairHand(4), True)

  def testBeats_twoPair(self):
    """ Tests that beats returns true for a two pair hand. """
    self._testBeats(TwoPairHand(4, 5), True)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns true for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(4), True)

  def testBeats_straight(self):
    """ Tests that beats return true for a straight hand. """
    self._testBeats(StraightHand(6), True)

  def testBeats_flush(self):
    """ Tests that beats return true for a flush hand. """
    self._testBeats(FlushHand(6, 'c'), True)

  def testBeats_fullHouse(self):
    """ Tests that beats return true for a full house hand. """
    self._testBeats(FullHouseHand(4, 5), True)

  def testBeats_smallerFourOfAKind(self):
    """ Tests that beats return true for a smaller four of a kind hand. """
    self._testBeats(FourOfAKindHand(3), True)

  def testBeats_sameFourOfAKind(self):
    """ Tests that beats return false for the same four of a kind hand. """
    self._testBeats(FourOfAKindHand(4), False)

  def testBeats_biggerFourOfAKind(self):
    """ Tests that beats return false for a bigger four of a kind hand. """
    self._testBeats(FourOfAKindHand(5), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 'c'), False)

  def testIsPresent_isPresent(self):
    """ Tests that is_present returns true if the hand is present. """
    self.assertTrue(self.hand.is_present([
        Card('4s'), Card('4c'), Card('4h'), Card('4d')]))

  def testIsPresent_isNotPresent(self):
    """ Tests that is_present returns false if the hand is not present. """
    self.assertFalse(self.hand.is_present([Card('4d'), Card('4c'), Card('4h')]))
    self.assertFalse(self.hand.is_present([
        Card('5d'), Card('5s'), Card('5c'), Card('5h')]))
    self.assertFalse(self.hand.is_present([]))
