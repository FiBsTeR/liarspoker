"""
Contains unit tests for the PairHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela), maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.tests.handtestcase import HandTestCase
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class PairHandTest(HandTestCase):
  """
  Unit tests for PairHand.
  """

  def setUp(self):
    """ Sets up for tests. """
    self.hand = PairHand(10)

  def testBeats_highCard(self):
    """ Tests that beats returns true for a high card hand. """
    self._testBeats(HighCardHand(13), True)

  def testBeats_smallerPair(self):
    """ Tests that beats returns true for a smaller pair hand. """
    self._testBeats(PairHand(5), True)

  def testBeats_samePair(self):
    """ Tests that beats returns false for the same pair hand. """
    self._testBeats(PairHand(10), False)

  def testBeats_biggerPair(self):
    """ Tests that beats returns false for a bigger pair hand. """
    self._testBeats(PairHand(14), False)

  def testBeats_twoPair(self):
    """ Tests that beats returns false for a two pair hand. """
    self._testBeats(TwoPairHand(4, 6), False)

  def testBeats_threeOfAKind(self):
    """ Tests that beats returns false for a three of a kind hand. """
    self._testBeats(ThreeOfAKindHand(7), False)

  def testBeats_straight(self):
    """ Tests that beats returns false for a straight hand. """
    self._testBeats(StraightHand(8), False)

  def testBeats_flush(self):
    """ Tests that beats returns false for a flush hand. """
    self._testBeats(FlushHand(9, 'd'), False)

  def testBeats_fullHouse(self):
    """ Tests that beats returns false for a full house hand. """
    self._testBeats(FullHouseHand(10, 2), False)

  def testBeats_fourOfAKind(self):
    """ Tests that beats returns false for a four of a kind hand. """
    self._testBeats(FourOfAKindHand(8), False)

  def testBeats_straightFlush(self):
    """ Tests that beats returns false for a straight flush hand. """
    self._testBeats(StraightFlushHand(8, 's'), False)

  def testIsPresent_isPresent(self):
    """
    Tests that is_present returns true for a set of cards containing exactly two
    of the pair card.
    """
    cards = [Card('Ts'), Card('Tc'), Card('2s'), Card('3s'), Card('4s')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isPresentMoreThanTwice(self):
    """
    Tests that is_present returns true for a set of cards containing more than
    two of the pair card.
    """
    cards = [Card('Ts'), Card('Tc'), Card('Th'), Card('Td')]
    self.assertTrue(self.hand.is_present(cards))

  def testIsPresent_isNotPresent(self):
    """
    Tests that is_present returns false for a set of cards not containing two of
    the pair cards.
    """
    self.assertFalse(self.hand.is_present([Card('Ts'), Card('Qs')]))
    self.assertFalse(self.hand.is_present([Card('4s'), Card('4c')]))

  def testIsPresent_empty(self):
    """
    Tests that is_present returns false for an empty set of cards.
    """
    self.assertFalse(self.hand.is_present([]))
