"""
Contains the definition of the StraightFlushHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class StraightFlushHand(Hand):
  """
  A hand containing a straight of the same suit.

  Attributes:
    value: The value of the top card of the straight flush.
    suit: The suit of the straight flush.
    _straight_mask: The number whose ith bit (from least to most significant) is
      set if and only if this value must be present in the straight flush. For
      example, for a 9-high straight flush, the mask is the number whose set
      bits are precisely those of the values of cards 5, 6, 7, 8, and 9.
  Requires:
    value >= 5
  """

  def __init__(self, value, suit):
    """ Override. """
    assert value >= 5, 'Invalid high card of straight flush.'
    self.hand_type = HandType.STRAIGHT_FLUSH
    self.suit = suit
    self.value = value
    # The low straight A, 2, 3, 4, 5 must be treated specially.
    self._straight_mask = (sum([1 << value - i for i in range(5)]) if value != 5
        else sum([1 << i for i in [14, 2, 3, 4, 5]]))

  def __repr__(self):
    """ Override. """
    return '<StraightFlush %d%s>' % (self.value, self.suit)

  def __str__(self):
    """ Override. """
    return '<StraightFlush %d%s>' % (self.value, self.suit)

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.value > other_hand.value)

  def is_present(self, cards):
    """ Override. """
    # present_cards is the number whose ith bit (from least to most significant)
    # is 1 if and only if the card with that value and this hand's flush suit is
    # present.
    present_cards = 0
    for card in cards:
      present_cards |= 1 << card.value if card.suit == self.suit else 0
    return present_cards & self._straight_mask == self._straight_mask
