"""
Contains the definition of the HighCardHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class HighCardHand(Hand):
  """
  A hand simply containing the given card.

  Attributes:
    value: The value of the high card.
  """

  def __init__(self, value):
    """ Override. """
    self.hand_type = HandType.HIGH_CARD
    self.value = value

  def __repr__(self):
    """ Override. """
    return '<HighCard %d>' % self.value

  def __str__(self):
    """ Override. """
    return '<HighCard %d>' % self.value

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or self.hand_type ==
        other_hand.hand_type and self.value > other_hand.value)

  def is_present(self, cards):
    """ Override. """
    return any([card.value == self.value for card in cards])
