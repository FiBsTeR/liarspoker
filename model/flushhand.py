"""
Contains the definition of the FlushHand class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class FlushHand(Hand):
  """
  A hand containing five cards of the same suit.

  Attributes:
    value: The value of the top card of the flush.
  """

  def __init__(self, value, suit):
    """ Override. """
    self.hand_type = HandType.FLUSH
    self.suit = suit
    self.value = value

  def __repr__(self):
    """ Override. """
    return '<Flush %d%s>' % (self.value, self.suit)

  def __str__(self):
    """ Override. """
    return '<Flush %d%s>' % (self.value, self.suit)

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.value > other_hand.value)

  def is_present(self, cards):
    """ Override. """
    top_card = 0
    suit_count = 0
    for card in cards:
      # Only accept cards that are below the top card in the flush as designated
      # by self.value
      if card.suit == self.suit and card.value <= self.value:
        if card.value >= top_card:
          top_card = card.value
        suit_count = suit_count + 1
    return suit_count >= 5 and top_card == self.value
