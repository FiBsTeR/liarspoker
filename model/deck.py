"""
Contains the definition of the Deck class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.card import Card
from random import shuffle


class Deck(object):
  """
  Represents a collection of cards which can be shuffled and dealt.

  Attributes:
    cards: An array of the cards in the deck.
  """

  def __init__(self):
    """ Constructor. Creates a new deck containing exactly one of each card. """
    self.cards = []
    for suit in Card.SUITS:
      for value in Card.VALUES:
        description = '%s%s' % (value, suit)
        self.cards.append(Card(description))

  def shuffle(self):
    """ Shuffles the deck. """
    shuffle(self.cards)

  def draw(self):
    """ Pops off and returns the top card of the deck. """
    assert len(self.cards) > 0, 'Cannot draw from an empty deck.'
    return self.cards.pop()
