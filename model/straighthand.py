"""
Contains the definition of the StraightHand class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.model.card import Card
from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType
from sets import Set


class StraightHand(Hand):
  """
  A hand containing five cards of sequential rank.

  Attributes:
    value: The value of the top card in the straight.

  Requires:
    value >= 5
  """

  def __init__(self, value):
    """ Override. """
    assert value >= 5, 'Invalid high card for straight.'
    self.hand_type = HandType.STRAIGHT
    self.value = value

  def __repr__(self):
    """ Override. """
    return '<Straight %d>' % self.value

  def __str__(self):
    """ Override. """
    return '<Straight %d>' % self.value

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.value > other_hand.value)

  def is_present(self, cards):
    """ Override. """
    bitstring = 0
    # Bit at index i (from right to left) of bitstring is 1 if a card of value
    # i - 2 exists in the hand
    for card in cards:
      bitstring = bitstring | 1 << card.value - 2
    # Handle special case of a low ace straight
    if bitstring == 0b1000000001111 and self.value == 5:
      return True
    # The magic value of 6 comes from a subtraction of 2 from the value to get
    # 0 indexed and another 4 for the beginning of the straight
    straight_mask = sum([1 << i + self.value - 6 for i in range(5)])
    # All of the bits in straight_mask were found in bitstring. Therefore, we
    # have found 5 consecutive 1's: a straight
    return bitstring & straight_mask == straight_mask
