"""
Contains the definition of the Card class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class Card(object):
  """
  Represents a single card.

  Attributes:
    description: A 2-character string describing the card. The first character
      denotes the value (2-9, T, J, Q, K), the second denotes the suit
      (d, c, h, s). For example, '4c' describes the 4 of clubs, 'Qs' describes
      the queen of spades, and 'Ah' describes the ace of hearts.
    value: An integer representing the value of the card.
    suit: A character representing the suit of the card.
  """

  VALUES = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']
  SUITS = ['d', 'c', 'h', 's']

  def __init__(self, description):
    """ Constructor. """
    assert len(description) == 2, 'Card description has invalid length.'
    assert description[0] in Card.VALUES, 'Invalid card value.'
    assert description[1] in Card.SUITS, 'Invalid card suit.'
    self.description = description
    self.value = Card.VALUES.index(description[0]) + 2
    self.suit = description[1]

  def __repr__(self):
    """ Override. """
    return self.description

  def __str__(self):
    """ Override. """
    return self.description

  def clone(self):
    """ Returns a new card with the same value and suit. """
    return Card(self.description)
