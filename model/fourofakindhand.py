"""
Contains the definition of the FourOfAKindHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class FourOfAKindHand(Hand):
  """
  A hand containing at least four cards of a given value.

  Attributes:
    value: The value of the card in the four of a kind.
  """

  def __init__(self, value):
    """ Override. """
    self.hand_type = HandType.FOUR_OF_A_KIND
    self.value = value

  def __repr__(self):
    """ Override. """
    return '<FourOfAKind %d>' % self.value

  def __str__(self):
    """ Override. """
    return '<FourOfAKind %d>' % self.value

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.value > other_hand.value)

  def is_present(self, cards):
    """ Override. """
    return sum([1 for card in cards if card.value == self.value]) >= 4
