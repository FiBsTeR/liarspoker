"""
Contains the definition of the TwoPairHand class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class TwoPairHand(Hand):
  """
  A hand containing at least two cards of each of two distinct values.

  Attributes:
    big_value: The value of the bigger card in the two pair.
    small_value: The value of the smaller card in the two pair.

  Requires:
    big_value must be greater than small_value.
  """

  def __init__(self, value_one, value_two):
    """ Override. """
    assert value_one != value_two, ('TwoPairHand must be constructed with ' +
        'two distinct values.')
    self.hand_type = HandType.TWO_PAIR
    self.small_value = min(value_one, value_two)
    self.big_value = max(value_one, value_two)

  def __repr__(self):
    """ Override. """
    return '<TwoPair %d %d>' % (self.big_value, self.small_value)

  def __str__(self):
    """ Override. """
    return '<TwoPair %d %d>' % (self.big_value, self.small_value)

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.big_value > other_hand.big_value or
        self.hand_type == other_hand.hand_type and
            self.big_value == other_hand.big_value and self.small_value >
                other_hand.small_value)

  def is_present(self, cards):
    """ Override. """
    return (sum([1 for card in cards if card.value == self.big_value]) >= 2 and
        sum([1 for card in cards if card.value == self.small_value]) >= 2)
