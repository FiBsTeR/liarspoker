"""
Contains the model package, which contains the business logic for the entire
project.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'
