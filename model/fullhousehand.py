"""
Contains the definition of the FullHouse class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.model.hand import Hand
from liarspoker.model.hand import HandType


class FullHouseHand(Hand):
  """
  A hand containing at least three cards of one value and at least two cards of
  another value.

  Attributes:
    triple_value: The value for which there are at least 3 cards.
    pair_value: The value for which there are at least 2 cards.
  """

  def __init__(self, triple_value, pair_value):
    """ Override. """
    assert triple_value != pair_value, ('FullHouseHand must be constructed ' +
        'with two distinct values.')
    self.hand_type = HandType.FULL_HOUSE
    self.triple_value = triple_value
    self.pair_value = pair_value

  def __repr__(self):
    """ Override. """
    return '<FullHouse %d %d>' % (self.triple_value, self.pair_value)

  def __str__(self):
    """ Override. """
    return '<FullHouse %d %d>' % (self.triple_value, self.pair_value)

  def beats(self, other_hand):
    """ Override. """
    return (self.hand_type > other_hand.hand_type or
        self.hand_type == other_hand.hand_type and
            self.triple_value > other_hand.triple_value or
        self.hand_type == other_hand.hand_type and
            self.triple_value == other_hand.triple_value and
            self.pair_value > other_hand.pair_value)

  def is_present(self, cards):
    """ Override. """
    return (sum([card.value == self.triple_value for card in cards]) >= 3 and
        sum([card.value == self.pair_value for card in cards]) >= 2)
