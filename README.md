Liars Poker
===========
[Liar's poker](http://en.wikipedia.org/wiki/Liar's_Poker) is a card game. This project models the game and provides an interface through which to write bots for the game.

Authors
-------
jven@mit.edu (Justin Venezuela)
maxnelso@mit.edu (Max Nelson)
