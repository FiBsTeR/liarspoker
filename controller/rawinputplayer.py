"""
Contains the definition of the RawInputPlayer class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.controller.dealernotice import Resign
from liarspoker.controller.dealernotice import SendBid
from liarspoker.controller.dealernotice import SendCall
from liarspoker.controller.player import Player
from liarspoker.controller.playernotice import PlayerNoticeType
from liarspoker.model.flushhand import FlushHand
from liarspoker.model.fourofakindhand import FourOfAKindHand
from liarspoker.model.fullhousehand import FullHouseHand
from liarspoker.model.highcardhand import HighCardHand
from liarspoker.model.pairhand import PairHand
from liarspoker.model.straightflushhand import StraightFlushHand
from liarspoker.model.straighthand import StraightHand
from liarspoker.model.threeofakindhand import ThreeOfAKindHand
from liarspoker.model.twopairhand import TwoPairHand


class RawInputPlayer(Player):
  """
  An implementation of Player which reads input from stdin and constructs and
  sends notices to the dealer accordingly.
  """

  def __init__(self, pid, name):
    """ Override. """
    self.pid = pid
    self.name = name
    self.dealer = None

  def add_to_game(self, dealer):
    """ Override. """
    self.dealer = dealer

  def notify(self, notice):
    """ Override. """
    assert self.dealer is not None, ('Inappropriate receipt of notice when ' +
        'player is not in game.')
    if notice.notice_type == PlayerNoticeType.START_GAME:
      self._print('Starting game with players: %s. You have %d cards: %s' %
          (notice.pids, len(notice.your_cards), notice.your_cards))
    elif notice.notice_type == PlayerNoticeType.RECEIVED_BID:
      self._print('Player %d bid hand %s.' % (notice.pid, notice.hand))
    elif notice.notice_type == PlayerNoticeType.RECEIVED_CALL:
      self._print('Player %d called.' % notice.pid)
    elif notice.notice_type == PlayerNoticeType.RECEIVED_RESIGN:
      self._print('Player %d resigned.' % notice.pid)
    elif notice.notice_type == PlayerNoticeType.YOUR_TURN:
      self._print('It\'s my turn!')
      dealer_notice = None
      while dealer_notice is None:
        dealer_notice = self._get_dealer_notice()
      self.dealer.notify(dealer_notice)
    elif notice.notice_type == PlayerNoticeType.END_TURN:
      self._print('My turn is over.')
    elif notice.notice_type == PlayerNoticeType.REVEAL_CARDS:
      self._print('Cards are revealed: %s' % notice.cards)
    elif notice.notice_type == PlayerNoticeType.END_GAME:
      self._print('Game over: \'%s\'' % notice.reason)

  def _get_dealer_notice(self):
    """
    Reads and returns a dealer notice deserialized from stdin, or None upon any
    unexpected input.
    """
    raw_notice = raw_input('[%s] >>> ' % self.name)
    tokens = raw_notice.lower().split()
    if len(tokens) == 0 or tokens[0] not in ['bid', 'call', 'resign']:
      self._print('Try \'bid <handtype> <handargs>\' or \'call\' or ' +
          '\'resign\'.')
    elif tokens[0] == 'bid':
      if len(tokens) == 3 and tokens[1] == 'highcard':
        return SendBid(self.pid, HighCardHand(int(tokens[2])))
      if len(tokens) == 3 and tokens[1] == 'pair':
        return SendBid(self.pid, PairHand(int(tokens[2])))
      if len(tokens) == 4 and tokens[1] == 'twopair':
        return SendBid(self.pid, TwoPairHand(int(tokens[2]), int(tokens[3])))
      if len(tokens) == 3 and tokens[1] == 'threeofakind':
        return SendBid(self.pid, ThreeOfAKindHand(int(tokens[2])))
      if len(tokens) == 3 and tokens[1] == 'straight':
        return SendBid(self.pid, StraightHand(int(tokens[2])))
      if len(tokens) == 4 and tokens[1] == 'flush' and len(tokens[3]) == 1:
        return SendBid(self.pid, FlushHand(int(tokens[2]), tokens[3]))
      if len(tokens) == 4 and tokens[1] == 'fullhouse':
        return SendBid(self.pid, FullHouseHand(int(tokens[2]), int(tokens[3])))
      if len(tokens) == 3 and tokens[1] == 'fourofakind':
        return SendBid(self.pid, FourOfAKindHand(int(tokens[2])))
      if (len(tokens) == 4 and tokens[1] == 'straightflush' and
          len(tokens[3]) == 1):
        return SendBid(self.pid, StraightFlushHand(int(tokens[2]), tokens[3]))
      self._print('Bid what?')
    elif tokens[0] == 'call':
      if len(tokens) == 1:
        return SendCall(self.pid)
      self._print('Call takes in no args.')
    elif tokens[0] == 'resign':
      if len(tokens) == 1:
        return Resign(self.pid)
      self._print('Resign takes in no args.')
    return None

  def _print(self, message):
    """ Prints a message to stdout. """
    print '[%s]: %s' % (self.name, message)
