"""
Contains the definition of the DealerImpl class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.controller.dealer import Dealer
from liarspoker.controller.dealernotice import DealerNoticeType
from liarspoker.controller.playernotice import EndGame
from liarspoker.controller.playernotice import EndTurn
from liarspoker.controller.playernotice import ReceivedBid
from liarspoker.controller.playernotice import ReceivedCall
from liarspoker.controller.playernotice import ReceivedResign
from liarspoker.controller.playernotice import RevealCards
from liarspoker.controller.playernotice import StartGame
from liarspoker.controller.playernotice import YourTurn
from liarspoker.model.deck import Deck


class DealerState(object):
  """ An enumeration of the states of the dealer. """
  PREGAME = 0
  PROCESSING = 1
  WAITING = 2
  POSTGAME = 3


class DealerImpl(Dealer):
  """
  The server's implementation of the Dealer interface.

  Attributes:
    state: A DealerState, representing the current state of the dealer.
    deck: The deck to use for this game.
    players: The immutable list of players playing the game. The order of this
      list determines turn order, starting with the first player in the list.
    num_cards: An immutable map from player ID to the number of cards held by
      that player.
    cards: A map from player ID to the list of cards held by that player, or
      None if this is not currently applicable.
    current_turn: The index of the player in the players array corresponding to
      the player whose turn it is, or -1 if the game has not started.
    current_bid: The hand currently being bid, or None if none exists.
    notice_buffer: A buffer of messages to process while waiting. Messages
      received while processing should be placed in the buffer for later
      processing.

  Requires:
    No two players have the same player ID (pid).
    The sum of the numbers of cards for each player cannot exceed the total
      number of cards in the deck.
  """

  def __init__(self, players, num_cards):
    """ Override. """
    assert len(players) >= 2, 'Cannot start a game with fewer than 2 players.'
    for player_one in players:
      for player_two in players:
        assert player_one == player_two or player_one.pid != player_two.pid, (
            'No two players can have the same player ID.')
    for player in players:
      assert player.pid in num_cards, ('Must specify a number of cards for ' +
          'all players.')
    self.deck = Deck()
    assert (sum([num_cards[player.pid] for player in players]) <=
        len(self.deck.cards)), 'Not enough cards for the players in the game. '
    self.players = players
    self.num_cards = num_cards
    self.cards = None
    self.current_turn = -1
    self.current_bid = None
    self.state = DealerState.PREGAME
    self.notice_buffer = []

  # TODO(jven): Find a way to ensure that the notice is sent by the player
  # whose pid is in the notice, if appropriate.
  def notify(self, notice):
    """ Override. """
    # If the dealer is busy processing another notice, put this notice in the
    # buffer for later processing.
    if self.state == DealerState.PROCESSING:
      self.notice_buffer.append(notice)
      return
    if notice.notice_type == DealerNoticeType.SEND_BID:
      self._handle_send_bid(notice)
    elif notice.notice_type == DealerNoticeType.SEND_CALL:
      self._handle_send_call(notice)
    elif notice.notice_type == DealerNoticeType.RESIGN:
      self._handle_resign(notice)
    # The dealer should either be waiting or in the post game state after
    # handling a notice.
    assert (self.state == DealerState.WAITING or
        self.state == DealerState.POSTGAME), ('Inappropriate dealer state ' +
        'after handling notice.')

  def play(self):
    """ Override. """
    assert self.state == DealerState.PREGAME, ('Inappropriate dealer state ' +
        'when calling play.')
    self.state = DealerState.PROCESSING
    self.deck.shuffle()
    self.cards = {}
    for player in self.players:
      num_cards = self.num_cards[player.pid]
      self.cards[player.pid] = [self.deck.draw() for i in range(num_cards)]
      player.notify(StartGame(
          [p.pid for p in self.players],                      # pids
          [self.num_cards[p.pid] for p in self.players],      # num_cards
          player.pid,                                         # your_id
          [card.clone() for card in self.cards[player.pid]])) # your_cards
    self._next_turn()

  def _handle_send_bid(self, notice):
    """ Handles the SEND_BID dealer notice. """
    # Only accept bids sent by the player whose turn it is.
    if (not self.state == DealerState.WAITING or
        notice.pid != self.players[self.current_turn].pid):
      return
    self.state = DealerState.PROCESSING
    if (self.current_bid is not None and
        not notice.hand.beats(self.current_bid)):
      self._end_game(('Player ID %d failed to bid a stronger hand than the ' +
          'current bid.') % self.players[self.current_turn].pid)
      return
    self.current_bid = notice.hand
    # Notify players of new bid.
    for player in self.players:
      # TODO(jven): Make a copy of the hand.
      player.notify(ReceivedBid(
          player.pid,        # pid
          self.current_bid)) # hand
    self._next_turn()

  def _handle_send_call(self, notice):
    """ Handles the SEND_CALL dealer notice. """
    if (not self.state == DealerState.WAITING or
        notice.pid != self.players[self.current_turn].pid):
      return
    self.state = DealerState.PROCESSING
    if self.current_bid is None:
      self._end_game('Player ID %d called when there was no current bid.' % (
          self.players[self.current_turn].pid))
      return
    for player in self.players:
      player.notify(ReceivedCall(notice.pid))
    # TODO(jven): Determine if the call was correct.
    self._end_game('Player ID %d called.' % self.players[self.current_turn].pid)

  def _handle_resign(self, notice):
    """ Handles the RESIGN dealer notice. """
    # TODO(jven): Determine the desired behavior when the dealer receives a
    # resignation while processing.
    if (not self.state == DealerState.WAITING or
        notice.pid != self.players[self.current_turn].pid):
      return
    self.state = DealerState.PROCESSING
    for player in self.players:
      player.notify(ReceivedResign(notice.pid))
    self._end_game('Player ID %d resigned.' %
        self.players[self.current_turn].pid)

  def _next_turn(self):
    """
    Ends the current player's turn and advances to the next player's turn if
    it is currently someone's turn, or sets the first player's turn otherwise.
    This should only be called when processing.
    """
    assert self.state == DealerState.PROCESSING, ('Inappropriate call to ' +
        '_next_turn.')
    if self.current_turn != -1:
      self.players[self.current_turn].notify(EndTurn())
    self.current_turn = (self.current_turn + 1) % len(self.players)
    self.players[self.current_turn].notify(YourTurn())
    self.state = DealerState.WAITING
    # Process notices in the buffer.
    if len(self.notice_buffer) > 0:
      self.notify(self.notice_buffer.pop(0))

  def _end_game(self, reason):
    """
    Ends the current player's turn, reveals all players' cards to all players,
    then notifies all players that the game has ended for the given reason. This
    should only be called when processing.
    """
    assert self.state == DealerState.PROCESSING, ('Inappropriate call to ' +
        '_end_game.')
    if self.current_turn != -1:
      self.players[self.current_turn].notify(EndTurn())
    self.current_turn = -1
    for player in self.players:
      cards_clone = {}
      for pid in self.cards:
        cards_clone[pid] = [card.clone() for card in self.cards[pid]]
      player.notify(RevealCards(cards_clone))
      player.notify(EndGame(reason))
    self.state = DealerState.POSTGAME
