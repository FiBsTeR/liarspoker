"""
Contains the definition of the DealerNotice class, its subclasses, as well as
the DealerNoticeType enum.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class DealerNoticeType(object):
  """ An enumeration of the types of dealer notices. """
  SEND_BID = 0
  SEND_CALL = 1
  RESIGN = 2


class DealerNotice(object):
  """
  An abstract representation of a message passed from a player to the dealer.

  Attributes:
    notice_type: A DealerNoticeType, representing the type of this notice.

  Requires:
    If two dealer notices have the same notice_type, they must both be instances
    of the same subclass of DealerNotice.
  """

  def __init__(self):
    """ Abstract constructor. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')


class SendBid(DealerNotice):
  """
  The message sent when making a bid.

  Attributes:
    pid: The player making the bid.
    hand: The hand to bid.
  """

  def __init__(self, pid, hand):
    """ Override. """
    self.notice_type = DealerNoticeType.SEND_BID
    self.pid = pid
    self.hand = hand


class SendCall(DealerNotice):
  """
  The message sent when making a call.

  Attributes:
    pid: The player making the call.
  """

  def __init__(self, pid):
    """ Override. """
    self.notice_type = DealerNoticeType.SEND_CALL
    self.pid = pid


class Resign(DealerNotice):
  """
  The message sent when resigning.

  Attributes:
    pid: The player resigning.
  """

  def __init__(self, pid):
    """ Override. """
    self.notice_type = DealerNoticeType.RESIGN
    self.pid = pid
