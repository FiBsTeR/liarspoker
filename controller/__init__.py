"""
Contains the controller package, which contains the controller logic for the
project.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'
