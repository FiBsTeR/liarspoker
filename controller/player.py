"""
Contains the definition of the Player interface.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class Player(object):
  """
  An interface through which the dealer interacts with the players of the game.

  Attributes:
    pid: The player's ID.
    name: The player's name.
    dealer: The dealer for the game in which the player is playing, or None if
      the player has not yet been added to a game.
  """

  def __init__(self):
    """ Abstract constructor. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')

  def add_to_game(self, dealer):
    """ Attaches the player to the given dealer. """
    raise Exception('Inappropriate call to abstract method.')

  def notify(self, notice):
    """ Sends a player notice to the player. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')
