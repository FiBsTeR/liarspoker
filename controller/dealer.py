"""
Contains the definition of the Dealer interface.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class Dealer(object):
  """
  An interface for a dealer, which handles the state of the game as well as
  communication between the players.
  """

  def __init__(self):
    """ Abstract constructor. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')

  def notify(self, notice):
    """ Sends a dealer notice to the dealer. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')

  def play(self):
    """ Starts the game. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')
