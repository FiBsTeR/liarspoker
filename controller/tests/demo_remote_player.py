"""
Contains the definitions of the LiarsPokerTCPHandler and DemoRemotePlayer
classes
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from SocketServer import BaseRequestHandler, TCPServer, ThreadingMixIn
from threading import current_thread, Thread


class ThreadedTCPRequestHandler(BaseRequestHandler):
  """ An example TCP handler. """
  # Test value to be set by sent packet

  def handle(self):
    data = str(self.request.recv(1024))
    cur_thread = current_thread()
    self.request.sendall("response")

class ThreadedTCPServer(ThreadingMixIn, TCPServer):
    pass

class DemoRemotePlayer(object):
  """ An example remote player. """
  HOST = "localhost"
  PORT = 9998

  # Create the server, binding to localhost on port 9998
  server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
  ip, port = server.server_address

  # Start a thread with the server -- that thread will then start one
  # more thread for each request
  server_thread = Thread(target=server.serve_forever)

  # Exit the server thread when the main thread terminates
  server_thread.daemon = True
  server_thread.start()
