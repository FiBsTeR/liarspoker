"""
Contains unit tests for the PostOfficeImpl class.
"""

__author__ = 'maxnelso@mit.edu (Max Nelson)'

from liarspoker.controller.post_officeimpl import PostOfficeImpl
from liarspoker.controller.tests.demo_remote_player import \
  DemoRemotePlayer, ThreadedTCPRequestHandler
from time import sleep
from unittest import TestCase


class PostOfficeImplTest(TestCase):
  """
  Unit tests for PostOfficeImpl.
  """

  def testSend(self):
    """
    Tests sending a message using the PostOfficeImpl.
    """
    demo_player = DemoRemotePlayer()
    post_office = PostOfficeImpl("localhost", 9998)
    response = post_office.send("hallo")
    self.assertTrue(str(response) == "response")
    # Cleanup
    demo_player.server.shutdown()
