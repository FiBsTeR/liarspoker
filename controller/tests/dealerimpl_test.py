"""
Contains unit tests for the DealerImpl class.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from unittest import TestCase


# TODO(jven): Implement these tests! :)
class DealerImplTest(TestCase):
  """
  Unit tests for DealerImpl.
  """

  def testConstructor(self):
    """
    Tests that, given correct arguments, the constructor creates a dealer in the
    PREGAME state with a full deck.
    """
    pass

  def testConstructor_fewPlayers(self):
    """
    Tests that the constructor raises an assertion error if passed fewer than
    two players.
    """
    pass

  def testConstructor_samePid(self):
    """
    Tests that the constructor raises an assertion error if given two players
    with the same PID.
    """
    pass

  def testConstructor_tooManyCards(self):
    """
    Tests that the constructor raises an assertion error if told to deal more
    cards than there are in the deck.
    """
    pass

  def testConstructor_ignoreNotices(self):
    """
    Tests that a newly constructed dealer does not change state upon being sent
    to notices.
    """
    pass

  def testPlay(self):
    """
    Tests that calling play correctly puts the dealer in the WAITING state,
    notifies all players that the game has started, notifies the first player
    that it is her turn, and draws the appropriate number of cards for each
    player.
    """
    pass

  def testNotify_playerNotInGame(self):
    """
    Tests that the dealer does not change state when sent notices by players not
    in the game.
    """
    pass

  def testNotify_sendBid(self):
    """
    Tests that, upon the first player sending a bid to the dealer, the dealer
    sets the current bid, notifies the first player that his turn has ended,
    notifies all players of the bid, iterates to the next player, and notifies
    the next player that it is her turn.
    """
    pass

  def testNotify_sendBidFromWrongPlayer(self):
    """
    Tests that if the second player sends a bid when it is the first player's
    turn, the dealer does not change state and no notifications are sent to any
    players.
    """
    pass

  def testNotify_sendWeakerBid(self):
    """
    Tests that if the first player sends a bid and the second player sends a
    weaker bid, then the game ends and cards are revealed.
    """
    pass

  def testNotify_sendCall(self):
    """
    Tests that if the first player sends a bid and the second player calls, thenww
    the second player is notified that her turn is over, all players are
    notified of the call, the game ends, and cards are revealed.
    """
    pass

  def testNotify_sendCallFromWrongPlayer(self):
    """
    Tests that if the first player sends a bid and then a call when it is the
    second player's turn, then the dealer does not change state and is still
    waiting on the second player.
    """
    pass

  def testNotify_sendCallNoBid(self):
    """
    Tests that if the first player sends a call when no hand has been previously
    bid, then the game ends and cards are revealed.
    """
    pass

  def testNotify_sendResign(self):
    """
    Tests that if the first player resigns, then the players are notified of the
    resignation, the game ends, and cards are revealed.
    """
    pass

  def testNotify_sendResignFromOtherPlayer(self):
    """
    Tests that if the second player resigns when it is the first player's turn,
    then the players are notified of the resignation, the game ends, and the
    cards are revealed.
    """
    pass
