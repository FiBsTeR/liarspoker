"""
Contains the definition of the PlayerNotice class, its subclasses, as well as
the PlayerNoticeType enum.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'


class PlayerNoticeType(object):
  """ An enumeration of the types of player notices. """
  START_GAME = 0
  RECEIVED_BID = 1
  RECEIVED_CALL = 2
  RECEIVED_RESIGN = 3
  YOUR_TURN = 4
  END_TURN = 5
  REVEAL_CARDS = 6
  END_GAME = 7


class PlayerNotice(object):
  """
  An abstract representation of a message passed from the dealer to the player.

  Attributes:
    notice_type: A PlayerNoticeType, representing the type of this notice.

  Requires:
    If two player notices have the same notice_type, they must both be instances
    of the same subclass of PlayerNotice.
  """

  def __init__(self):
    """ Abstract constructor. Must be overridden. """
    raise Exception('Inappropriate call to abstract method.')


class StartGame(PlayerNotice):
  """
  The message sent when the game begins.

  Attributes:
    pids: The player IDs of the players in the game.
    num_cards: A map from player ID to the number of cards that player has.
    your_pid: The player's ID.
    your_cards: An array of the player's cards for this game.
  """

  def __init__(self, pids, num_cards, your_pid, your_cards):
    """ Override. """
    self.notice_type = PlayerNoticeType.START_GAME
    self.pids = pids
    self.num_cards = num_cards
    self.your_pid = your_pid
    self.your_cards = your_cards


class ReceivedBid(PlayerNotice):
  """
  The message sent when a player sends a bid to the dealer.

  Attributes:
    pid: The ID of the player who sent the bid.
    hand: The hand bid by the player.
  """

  def __init__(self, pid, hand):
    """ Override. """
    self.notice_type = PlayerNoticeType.RECEIVED_BID
    self.pid = pid
    self.hand = hand


class ReceivedCall(PlayerNotice):
  """
  The message sent when a player sends a call to the dealer.

  Attributes:
    pid: The ID of the player who sent the call.
  """

  def __init__(self, pid):
    """ Override. """
    self.notice_type = PlayerNoticeType.RECEIVED_CALL
    self.pid = pid


class ReceivedResign(PlayerNotice):
  """
  The message sent when a player resigns.

  Attributes:
    pid: The ID of the player who resigned.
  """

  def __init__(self, pid):
    """ Override. """
    self.notice_type = PlayerNoticeType.RECEIVED_RESIGN
    self.pid = pid


class YourTurn(PlayerNotice):
  """ The message sent to a player when it is that player's turn to move. """

  def __init__(self):
    """ Override. """
    self.notice_type = PlayerNoticeType.YOUR_TURN


class EndTurn(PlayerNotice):
  """
  The message sent to a player when the dealer acknowledges that that player's
  turn is over.
  """

  def __init__(self):
    """ Override. """
    self.notice_type = PlayerNoticeType.END_TURN


class RevealCards(PlayerNotice):
  """
  The message sent when the dealer reveals the cards of all players.

  Attributes:
    cards: A map from player ID to the cards held by that player.
  """

  def __init__(self, cards):
    """ Override. """
    self.notice_type = PlayerNoticeType.REVEAL_CARDS
    self.cards = cards


class EndGame(PlayerNotice):
  """
  The message sent when the game is over.

  Attributes:
    reason: A string containing the reason the game ended.
  """

  def __init__(self, reason):
    """ Override. """
    self.notice_type = PlayerNoticeType.END_GAME
    self.reason = reason
