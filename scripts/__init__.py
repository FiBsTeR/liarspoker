"""
Contains the scripts package, which contains helpful tools for developing the
project but are not necessary for it to run.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'
