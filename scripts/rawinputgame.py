"""
Creates and plays a game with raw input players.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.controller.dealerimpl import DealerImpl
from liarspoker.controller.rawinputplayer import RawInputPlayer


def main():
  """ Main. """
  player_names = ['Justin', 'Max', 'Michael', 'Paul']
  players = [RawInputPlayer(i * i * i, player_names[i])
      for i in range(len(player_names))]
  num_cards = {}
  for i in range(len(player_names)):
    num_cards[players[i].pid] = i + 1
  dealer = DealerImpl(players, num_cards)
  for player in players:
    player.add_to_game(dealer)
  dealer.play()

if __name__ == '__main__':
  main()
