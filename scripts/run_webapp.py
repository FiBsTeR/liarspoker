"""
Contains a script to run the dealer web app.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from liarspoker.dealerapp.dealerapp import DealerApp


def main():
  port = 5050
  debug = True
  app = DealerApp(port, debug)
  app.run()

if __name__ == '__main__':
  main()
