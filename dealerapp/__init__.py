"""
Contains the dealerapp package, which contains the web application which
interfaces between web players and the dealer.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'
