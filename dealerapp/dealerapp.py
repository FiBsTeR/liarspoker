"""
Contains the definition of the Flask web application which wraps around the
dealer.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from flask import Flask


class DealerApp():
  """
  A Flask application which wraps around the dealer. Abstractly, it exposes only
  a run method which runs the application on the specified port.

  Attributes:
    port: An integer representing the port on which to run the app.
    debug: Whether to run the app in debug mode.
  """

  def __init__(self, port, debug):
    self.port = port
    self.debug = debug

    app = Flask(__name__)

    @app.route('/')
    def _home():
      return 'Hello, world!'

    self.app = app

  def run(self):
    self.app.run(host='0.0.0.0', port=self.port, debug=self.debug)
